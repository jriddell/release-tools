#!/bin/bash

. utils.sh
. config

repo_to_pack=$1
force=$2

if [ -z "$repo_to_pack" ]; then
    echo "No repo given"
    exit
fi

unset CDPATH

mkdir -p sources
mkdir -p versions

function determineVersion() {
  . version
  versionFilePath=$PWD/versions/$repo_to_pack
  tarFile=$repo_to_pack-$version.tar.xz
  echo $tarFile
}

cat modules.git | while read repoid branch; do
    if [ "$repo_to_pack" = "$repoid" ]; then
        repoLine="$repoid $branch"

        repo=$(get_repo_path $repoid)
        if [ -z "${repo}" ]; then
            echo "Error while retrieving the repository path for $repoid" >/dev/stderr
            continue
        fi

        determineVersion
        checkDownloadUptodate "sources/$tarFile"
        uptodate=$?
        if [ $uptodate = 1 ]; then
            echo "$repoid is already up to date, no need to re-download. Use -f as second parameter if you want to force"
            break;
        fi

        checkout=1
        echo "$repoLine"
        echo "$repoLine" > $versionFilePath
        while [ $checkout -eq 1 ]; do
            rev=`get_git_rev`
            basename=$repoid-$version
            cd sources
            git archive --remote=kde:$repo $branch --prefix $basename/ | tar x
            errorcode=$PIPESTATUS # grab error code from git archive
            if [ $errorcode -eq 0 ]; then
                rev2=`get_git_rev`

                if [ $rev = $rev2 ]; then
                    checkout=0
                    # Only for frameworks: grab translations and put them into the tarball
                    if [ -f "$basename/$repoid.yaml" ]; then
                        grabTranslations "$basename" "$repoid"
                    fi
                    if [ "$repoid" != "kde-runtime" ] && [ "$repoid" != "kdelibs" ]; then
                        # kde-runtime, kdelibs, umbrello are Qt4-only and special, exclude them.
                        setupGUITranslations "$basename" "$l10n_basedir5" "$repoid" "../language_list"
                        setupDOCTranslations "$basename" "$l10n_basedir5" "$repoid" "../language_list"
                        setupDataTranslations "$basename" "$l10n_basedir5" "$repoid" "../language_list"
                    fi

                    tar c --owner 0 --group 0 --numeric-owner $basename | xz -9 > $tarFile

                    if [ $make_zip -eq 1 ]; then
                      zip -r $basename.zip $basename || exit 1
                    fi

                else
                    # someone made a change meanwhile, retry
                    rm -f $tarFile
                fi
                rm -rf $basename
            else
                echo "git archive --remote=kde:$repo $branch --prefix $basename/ failed with error code $errorcode"
            fi
            cd ..
        done
        echo "$rev"
        echo "$rev" >> $versionFilePath
        sha256sum sources/$tarFile >> $versionFilePath

        rm -f sources/$tarFile.sig
        gpg --digest-algo SHA512 --armor --detach-sign -o sources/$tarFile.sig -s sources/$tarFile
    fi
done

