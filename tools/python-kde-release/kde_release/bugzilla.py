# Copyright 2015, 2019 Jonathan Riddell <jr@jriddell.org>
# Copyright 2017, 2019 Adrian Chaves <adrian@chaves.io>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

from bs4 import BeautifulSoup
from click import echo
from requests import RequestException, Session

from kde_release.modules import update_modules


URL = 'https://bugs.kde.org'
EDIT_VERSION_URL = '{}/editversions.cgi'.format(URL)


def response_error(response):
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    error_message = soup.find('div', {'id': 'error_msg'})
    if not error_message:
        return None
    return re.sub(r'\s+', ' ', error_message.get_text()).strip()


def log_in(session, email, password):
    data = {'Bugzilla_login': email, 'Bugzilla_password': password}
    headers = {'Referer': 'https://bugs.kde.org/'}
    try:
        response = session.post(URL, data=data, headers=headers)
        error = response_error(response)
        if error:
            echo(error)
            exit(1)
    except RequestException:
        echo('Unexpected login error. Please, contact the maintainers of this '
             'script.')
        exit(1)


def bugzilla_csrf_token(session, product):
    params = {'action': 'add', 'product': product}
    response = session.get(EDIT_VERSION_URL, params=params)
    soup = BeautifulSoup(response.text, 'html.parser')
    try:
        return soup.find('input', {'name': 'token'})['value']
    except Exception:
        raise RuntimeError('Could not parse token from \'{}\''.format(
            response.url))


def add_version_to_bugzilla_project(session, product, version):
    params = {'version': version, 'action': 'new', 'product': product,
              'token': bugzilla_csrf_token(session, product)}
    response = session.get(EDIT_VERSION_URL, params=params)
    response.raise_for_status()
    error = response_error(response)
    if error:
        raise RuntimeError(error)


def add_versions(srcdir, email, password, dry, clone, hide_skipped):
    session = Session()
    if not dry:
        log_in(session, email, password)

    kwargs = {'clone': clone,
              'log_missing_versions': not hide_skipped}
    for directory, product, version, branch in update_modules(srcdir, **kwargs):
        if dry:
            echo(f'{product}\n'
                 f'\t(would have added {version})')
            continue
        try:
            add_version_to_bugzilla_project(session, product, version)
        except Exception as error:
            echo(f'{product}\n'
                 f'\t(would have added {version})\n'
                 f'\t\t(error: {repr(error)})')
        else:
            echo(f'{product}\n'
                 f'\t(added {version})')
